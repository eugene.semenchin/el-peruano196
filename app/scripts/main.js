// const { on } = require("modernizr");

console.log('Проверка JavaScript!');

// Бургер Меню //
$('.burger-menu-btn').on('click', function(e) {
  e.preventDefault;
  $(this).toggleClass('burger-menu-btn_active');
  $('.burger-menu-nav').toggleClass('burger-menu-nav_active');
});

// Слайдер новостей//
$('.autoplay').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 3000,
  arrows: false,
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 2,
        centerMode: true,
        centerPadding: '80px',
      }
    },
    {
      breakpoint: 850,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 2,
        centerMode: true,
        centerPadding: '20px',
      }
    },
    {
      breakpoint: 700,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '130px',
      }
    },
    {
      breakpoint: 620,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '80px',
      }
    },
    {
      breakpoint: 520,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '30px',
      }
    },
    {
      breakpoint: 420,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '15px',
      }
    },
    {
      breakpoint: 390,
      settings: {
        slidesToScroll: 1,
        slidesToShow: 1,
        centerMode: true,
        centerPadding: '0',
      }
    }
  ]
});

// Слайдер пациентов //
$('.single-item').slick({
  arrows: true,
  dots: true,
  appendArrows: $('.main-arrows'),
    prevArrow: '<img class="arrow-left" src="images/arrow-left.svg">',
    nextArrow: '<img class="arrow-right" src="images/arrow-right.svg">'
});

// Колесо призов //

var width = window.innerWidth;
var height = window.innerHeight;

Konva.angleDeg = false;
var angularVelocity = 20;
var angularVelocities = [];
var lastRotation = 0;
var controlled = false;
var numWedges = 8;
var angularFriction = 0.2;
var target, activeWedge, stage, layer, wheel, pointer;
var finished = false;

function getAverageAngularVelocity() {
  var total = 0;
  var len = angularVelocities.length;

  if (len === 0) {
    return 0;
  }

  for (var n = 0; n < len; n++) {
    total += angularVelocities[n];
  }

  return total / len;
}

function purifyColor(color) {
  var randIndex = Math.round(Math.random() * 3);
  color[randIndex] = 0;
  return color;
}

function getRandomColor() {
  var r = 10 + Math.round(Math.random() * 155);
  var g = 10 + Math.round(Math.random() * 155);
  var b = 10 + Math.round(Math.random() * 155);
  return purifyColor([r, g, b]);
}

function getRandomReward() {
  var mainDigit = Math.round(Math.random() * 8 + 1);
  return mainDigit + '%';
}

function addWedge(n) {
  var s = getRandomColor();
  var reward = getRandomReward();
  var r = s[0];
  var g = s[2];
  var b = s[1];
  var angle = (2 * Math.PI) / numWedges;

  var endColor = 'rgb(' + r + ',' + g + ',' + b + ')';
  r += 50;
  g += 50;
  b += 50;

  var startColor = 'rgb(' + r + ',' + g + ',' + b + ')';

  var wedge = new Konva.Group({
    rotation: (2 * n * Math.PI) / numWedges,
  });

  var wedgeBackground = new Konva.Wedge({
    radius: 300,
    angle: angle,
    fillRadialGradientStartPoint: 0,
    fillRadialGradientStartRadius: 0,
    fillRadialGradientEndPoint: 0,
    fillRadialGradientEndRadius: 300,
    fillRadialGradientColorStops: [0, startColor, 1, endColor],
    fill: '#64e9f8',
    fillPriority: 'radial-gradient',
    stroke: '#ccc',
    strokeWidth: 2,
  });

  wedge.add(wedgeBackground);

  var text = new Konva.Text({
    text: reward,
    fontFamily: 'Merriweather',
    fontSize: 60,
    fill: 'white',
    align: 'center',
    stroke: 'yellow',
    strokeWidth: 1,
    rotation: (Math.PI + angle) / 2,
    x: 250,
    y: 50,
    listening: false,
  });

  wedge.add(text);
  text.cache();

  wedge.startRotation = wedge.rotation();

  wheel.add(wedge);
}

function animate(frame) {
  // handle wheel spin
  var angularVelocityChange =
    (angularVelocity * frame.timeDiff * (1 - angularFriction)) / 1000;
  angularVelocity -= angularVelocityChange;

  // activate / deactivate wedges based on point intersection
  var shape = stage.getIntersection({
    x: stage.width() / 2,
    y: 80,
  });

  if (controlled) {
    if (angularVelocities.length > 10) {
      angularVelocities.shift();
    }

    angularVelocities.push(
      ((wheel.rotation() - lastRotation) * 1000) / frame.timeDiff
    );

  } else {
    var diff = (frame.timeDiff * angularVelocity) / 1000;
    if (diff > 0.0001) {
      wheel.rotate(diff);
    } else if (!finished && !controlled) {
      if (shape) {
        var text = shape.getParent().findOne('Text').text();
        var price = text.split('\n').join('');
        // alert('Поздравляем! Вы получаете скидку ' + price);
        renderResultWheel(price);
        document.querySelector( '.popup-bg' ).style.display = 'block',

      finished = true;
      }
    }
  }
  lastRotation = wheel.rotation();

  if (shape) {
    if (shape && (!activeWedge || shape._id !== activeWedge._id)) {
      pointer.y(20);

      new Konva.Tween({
        node: pointer,
        duration: 1.3,
        y: 30,
        easing: Konva.Easings.ElasticEaseOut,
      }).play();

      if (activeWedge) {
        activeWedge.fillPriority('radial-gradient');
      }
      shape.fillPriority('fill');
      activeWedge = shape;
    }
  }
}

function init() {
  stage = new Konva.Stage({
    container: 'wheel_of_fortune',
    width: 600,
    height: 610,

  });
  layer = new Konva.Layer();
  wheel = new Konva.Group({
    x: stage.width() / 2,
    y: 310,
  });

  for (var n = 0; n < numWedges; n++) {
    addWedge(n);
  }
  pointer = new Konva.Wedge({
    fillRadialGradientStartPoint: 0,
    fillRadialGradientStartRadius: 0,
    fillRadialGradientEndPoint: 0,
    fillRadialGradientEndRadius: 30,
    fillRadialGradientColorStops: [0, 'white', 1, 'red'],
    stroke: 'white',
    strokeWidth: 1,
    lineJoin: 'round',
    angle: 1,
    radius: 30,
    x: stage.width() / 2,
    y: 33,
    rotation: -90,
    shadowColor: 'black',
    shadowOffsetX: 3,
    shadowOffsetY: 3,
    shadowBlur: 3,
    shadowOpacity: 0.5,
  });

  // add components to the stage
  layer.add(wheel);
  layer.add(pointer);
  stage.add(layer);

  // bind events
  wheel.on('mousedown touchstart', function (evt) {
    angularVelocity = 0;
    controlled = true;
    target = evt.target;
    finished = false;
  });
  // add listeners to container
  stage.addEventListener(
    'mouseup touchend',
    function () {
      controlled = false;
      angularVelocity = getAverageAngularVelocity() * 5;

      if (angularVelocity > 20) {
        angularVelocity = 20;
      } else if (angularVelocity < -20) {
        angularVelocity = -20;
      }

      angularVelocities = [];
    },
    false
  );

// Управление whell-mouse

  // stage.addEventListener(
  //   'mousemove touchmove',
  //   function (evt) {
  //     var mousePos = stage.getPointerPosition();
  //     if (controlled && mousePos && target) {
  //       var x = mousePos.x - wheel.getX();
  //       var y = mousePos.y - wheel.getY();
  //       var atan = Math.atan(y / x);
  //       var rotation = x >= 0 ? atan : atan + Math.PI;
  //       var targetGroup = target.getParent();

  //       wheel.rotation(
  //         rotation - targetGroup.startRotation - target.angle() / 2
  //       );
  //     }
  //   },
  //   false
  // );

  var anim = new Konva.Animation(animate, layer);

  document.getElementById('start-button').addEventListener('click', function() {
    anim.start();
  })

// Spin
  // wait one second and then spin the wheel
  // setTimeout(function () {
    // anim.start();
  // }, 2000);
}

function renderResultWheel(result) {
  document.getElementById('resultWheel').innerHTML = result;
}

init();

// Модальное окно - закрыть //

$('.close-popup').on('click', function(e){
  e.preventDefault();
  $('.popup-bg').hide();
})

// Маска для input

$(document).ready(function() {
  $('#phone').inputmask({
    mask: '+7(999) 999-9999',
    showMaskOnHover: false
  });
})
